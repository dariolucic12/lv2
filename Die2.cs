﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak2
{
    class Die
    {
        private int numberOfSides;
        private Random randomGenerator = new Random();
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }
        

        public int Roll()
        {
            int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            return rolledNumber;
        }

    }
}
