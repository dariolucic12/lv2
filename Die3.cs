﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zadatak3
{
    class Die
    {
        private int numberOfSides;
        RandomGenerator generator = RandomGenerator.GetInstance();
        
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
        }
        

        public int Roll()
        {
            int rolledNumber = generator.NextInt(1, numberOfSides);
            return rolledNumber;
        }
    }
}
