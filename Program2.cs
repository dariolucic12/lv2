﻿using System;

namespace zadatak2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller roller = new DiceRoller();
            Die[] die = new Die[21];

            for (int i = 1; i <= 20; i++) {
              die[i] = new Die(7);
              roller.InsertDie(die[i]);
            }
            roller.RollAllDice();
            roller.PrintList();
  
        }
    }
}
